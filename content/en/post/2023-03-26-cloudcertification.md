---
layout: post
title: "Tips and resources on getting cloud certification"
date: 2023-03-26
---

---

 In Nov 2022, I decided to get certified in AWS. This blog shares the resources I have gathered, and tips I have learned during my cloud certification journey. 

## Background required to get started with cloud certification

You don't need any prior knowledge to get started. You don't need to know any programming language. If you have some basic IT or web technology knowledge such as understanding HTTP/HTTPS, TCP/IP, and SSH, that is a plus. Even if you have no prior IT knowledge, as long as you are willing to learn and spend enough time and effort, you can pass an entry level certification such as [Certified AWS Cloud Practitioner](https://aws.amazon.com/certification/certified-cloud-practitioner/) in one month. 

## Which cloud certification to start with

The most popular cloud solutions are (statistics from [this post](https://aag-it.com/the-latest-cloud-computing-statistics/) as of March 2023):

1. [Amazon Web Services](https://aws.amazon.com/) (AWS) with about 33% market share
2. [Microsoft Azure](https://azure.microsoft.com/en-us/) with about 23% market share
3. [Google Cloud Platform](https://cloud.google.com/) (GCP) with about 11% market share

 As far as I know, GCP is still [not profitable](https://www.sdxcentral.com/articles/news/will-google-cloud-ever-be-profitable/2023/02/) even though Google has invested in it [since 2008](https://en.wikipedia.org/wiki/Google_Cloud_Platform). Probably I would want to skip GCP and focus on AWS or Azure. 

For AWS, the most popular and widely accepted certificate is [AWS Certified Solutions Architect - Associate](https://aws.amazon.com/certification/certified-solutions-architect-associate/). As of March 2023, AWS provides [12 certificates](https://aws.amazon.com/certification/exams/?nc2=sb_ce_exm). 

For Microsoft, the most popular certificate is [Azure Administrator Associate](https://learn.microsoft.com/en-us/certifications/azure-administrator/).

This post focuses on AWS. In general, it is better to stick with one company's solution and get an in-depth understanding before moving on to another company's solution. 

## How much does it cost and where to find the study materials

You need to budget two types of cost: 

* You pay an exam fee for cloud certification. For AWS, the exams cost 100 for entry level, 150 for mid level, and 300 for pro level. The good news is, after you pass one exam, you get a 50% off discount code for the next exam. 

* Many free study materials are available for entry and mid level exams. You may choose to buy recorded video courses, which may cost from 10 dollars to hundreds of dollars. See details below.

### Resources for the AWS Cloud Practitioner exam

* Estimated preparation time for newbies: from 1 week to 4 weeks. 
  
  It took me 2.5 weeks intensive study to pass the exam. If you can get 70-80% of the practice questions correct, you are ready for the test. 

**Free resources**

* Must study:  [AWS Cloud Practitioner official study path](https://aws.amazon.com/training/learn-about/cloud-practitioner/)
  
* Highly recommend:  [Free practice questions from Exam topics](https://www.examtopics.com/exams/amazon/aws-certified-cloud-practitioner/)

* Highly recommend: [Free practice questions on Youtube](https://youtu.be/IvvD13aNO68)
  
* Highly recommend: [Tutorials Dojo free cloud practitioner practice exams](https://portal.tutorialsdojo.com/courses/free-aws-certified-cloud-practitioner-practice-exams-sampler/)

* Recommend: [Digital Cloud AWS cheat sheets cloud practitioner](https://digitalcloud.training/category/aws-cheat-sheets/aws-cloud-practitioner/)

* [Youtube AWS Certified Cloud Practitioner full course from freecodecamp](https://www.youtube.com/watch?v=SOTamWNgDKc) I did not try this course so I cannot comment if it is good. I chose the 10 dollars course from Stephane Maarek on Udemy below. 


**Paid resources**

* Highly recommend: [Udemy Stephane Maarek AWS Certified Cloud Practitioner course](https://www.udemy.com/course/aws-certified-cloud-practitioner-new/). Wait until it is on sale for 9.99. 
  
### Resources for the AWS Solution Architect Associate exam

* Estimated preparation time for newbies: from 2 weeks up to 6 months. 
  
  It took me one month intensive study to pass the exam. If you can get 70-80% of the practice questions correct, you are ready for the test. 

**Free resources**

* Must study: [AWS solution architect associate official study path](https://aws.amazon.com/certification/certified-solutions-architect-associate/)

* Highly recommend:  [Free practice questions from Exam topics](https://www.examtopics.com/exams/amazon/aws-certified-solutions-architect-associate-saa-c03/)

* Highly recommend: [Free practice questions on Youtube](https://www.youtube.com/watch?v=bWjA4MagDSc&list=PL_0RK_1F4sTCdiXliNXqcRRnpiDiXhQLV&index=2)
  
* Recommend: [Digital Cloud AWS cheat sheets solution architect associate](https://digitalcloud.training/category/aws-cheat-sheets/aws-solutions-architect-associate/)

* Recommend: Official AWS FAQ pages, for example, [AWS EC2 FAQs](https://aws.amazon.com/ec2/faqs/) and [AWS S3 FAQs](https://aws.amazon.com/s3/faqs/).

* Recommend: Official [AWS Well-Architected Framework](https://docs.aws.amazon.com/pdfs/wellarchitected/latest/framework/wellarchitected-framework.pdf#welcome)

**Paid resources**

* Highly recommend: [Udemy Stephane Maarek AWS Solution Architect Associate course](https://www.udemy.com/course/aws-certified-solutions-architect-associate-saa-c03/). Wait until it is on sale for 9.99 or 12.99.
  
* Highly recommend: [Udemy Tutorials Dojo Solution Architect practice exams](https://www.udemy.com/course/aws-certified-solutions-architect-associate-amazon-practice-exams-saa-c03/). Wait until it is on sale for 9.99 or 12.99.  


**Other reputable paid training courses**

I mainly used Stephane Maarek's video courses and Tutorial Dojo's practice exams for preparation. The following two cloud training resources are also highly recommended by many AWS exam takers.

* [Adrian Cantril's Video Courses](https://learn.cantrill.io)
  
* [Neal Davis Digital Cloud Training](https://digitalcloud.training/)

## AWS certification tips 

* For people who are new with AWS, my suggestion is to start with the $100 entry-level Solution Cloud Practitioner exam, which is much easier to pass than the $150 dollar Solution Architect Associate exam. After you pass the Cloud Practitioner exam, you get a 50% off for the Architect Associate exam. This approach boosts your confidence. Compared with the approach to directly tackle the Solution Architect Associate, you only pay $25 extra and you get one more certificate. 
  
* If English is not your native language, when you register the exam, you can [request to get extra 30 minutes](https://aws.amazon.com/certification/policies/before-testing/) for the exam. 
  
* You can choose to take the AWS certification exam at home with a proctor monitoring you remotely or go to a test center. I did both [my Cloud Practitioner](https://www.credly.com/badges/5bd9a734-b3fa-4563-bd1a-3d7f3270d012/) and [Solution Architect Associate](https://www.credly.com/badges/f4c482ea-c4a9-4c59-8800-b082b8ff2758/) tests at home and both went well. Find a quiet place in your house and do the preparation requirements ahead of the time. 

* AWS certification runs different types of promotions throughout the year. For example, right now (March 2023), PearsonVue, which is the organization that runs the AWS tests, is providing a promotion on [free retake before May 31](https://home.pearsonvue.com/Clients/Amazon-Web-Services/free-retake.aspx).

* Register your exam ahead of the time and make your study plan according to the target exam date. This way you have a deadline to work with. You can reschedule the exam 2 times. After 2 reschedules, if you are still not ready, you can cancel the exam 24 hours prior the exam and get a full refund. 

* Each certificate is valid for three years. If you pass a higher level certificate and earlier you passed a lower level certificate, you automatically get the earlier certificate renewed. For example, if you pass the AWS Solutions Architect Professional exam on March 31 2023 and you passed the AWS Solutions Architect Associate exam in 2022, you automatically get your Solutions Architect Associate certificate expiration date extended to be the same date as your Solutions Architect Professional expiration date.  




