---
layout: post
title: "Technical Writer Core Competencies in the Age of Generative AI"
date: 2023-04-02
---

I asked the following question to ChatGPT and Bard and here are the answers. 

## Prompt 

Describe the core competencies of a technical writer in the age of generative AI. 

## ChatGPT

The core competency of a technical writer in the age of generative AI is the ability to create high-quality technical documentation that is accurate, clear, and easily understandable for the intended audience. While generative AI has the potential to automate some aspects of technical writing, such as generating basic documentation or user guides, it cannot replace the expertise and judgment of a skilled technical writer.

A technical writer's core competencies include:

* Technical expertise: A technical writer must have a deep understanding of the technology, software, or product they are documenting. They need to know how it works and be able to communicate complex technical information to non-technical users.

* Writing skills: Technical writers must have excellent writing skills and be able to write clear, concise, and engaging documentation. They must be able to organize information logically, write in a user-friendly style, and use appropriate language and terminology.

* User empathy: Technical writers must understand the needs of their audience and write documentation that meets their requirements. They must put themselves in the user's shoes and anticipate the questions and problems they may encounter.

* Collaboration: Technical writers often work closely with developers, designers, and other stakeholders to ensure that documentation is accurate and up-to-date. They must be able to collaborate effectively with others and manage feedback and revisions.

* Continuous learning: Technology is constantly evolving, and technical writers must stay up-to-date with the latest trends and developments. They must be willing to learn new tools and techniques, and continuously improve their skills.

In summary, while generative AI may be able to automate some aspects of technical writing, the core competencies of a skilled technical writer remain essential in ensuring that technical documentation is accurate, clear, and user-friendly.
## Bard

The core competency of a technical writer in the age of generative AI is the ability to understand and use generative AI tools to create high-quality, user-friendly technical documentation. This includes the ability to:

* Identify the needs of the target audience and create content that meets those needs.
  
* Understand the technical subject matter and translate it into clear, concise language.

* Use generative AI tools to create engaging and interactive content.
  
* Evaluate the quality of generated content and ensure that it is accurate and error-free.

* Collaborate with other stakeholders, such as subject-matter experts and engineers, to create effective technical documentation.

In addition to these core competencies, technical writers in the age of generative AI will also need to be able to:

* Stay up-to-date on the latest generative AI technologies and how they can be used to create technical documentation.

* Be able to think creatively and come up with new ways to use generative AI to improve technical documentation.

* Be able to work independently and take initiative.
  
* Be able to work under pressure and meet deadlines.

* Be able to communicate effectively with both technical and non-technical audiences.

The role of the technical writer is evolving as generative AI becomes more sophisticated. Technical writers will need to be able to use generative AI tools to create high-quality, user-friendly technical documentation. They will also need to be able to stay up-to-date on the latest generative AI technologies and how they can be used to improve technical documentation.

## My two cents

Both ChatGPT and Bard provided a solid summary of the key competencies required for technical writers to survive and thrive in the age of generative AI.

ChatGPT was able to generate more versions of the answers while Bard only gave me three versions. I like ChatGPT's answer better because it is more succinct and to the point while Bard's answer is more cliche and sounds more like from a template. 

Which answer do you like better?