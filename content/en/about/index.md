---
title: "About Me"
description: "I am a technical writer who is passionate about learning new technologies."
menu:
  main:
    weight: 1
---

I am a technical writer based in San Jose, California. I am passionate about learning new technologies. In my spare time, I like swimming and listening to podcasts or audio books.

I think the most important trait for a technical writer is to be curious and continuously willing to learn and willing to roll up the sleeves and play with the technologies. 

**Disclaimer: All opinions are mine and not of my employer.**