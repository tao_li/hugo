---
title: Contact
featured_image: ''
omit_header_text: true
description: We'd love to hear from you
type: page
menu: main
---

To contact me, send me email at [taolicd@gmail.com](taolicd@gmail.com) or on [my LinkedIn account](https://www.linkedin.com/in/taolicd/).